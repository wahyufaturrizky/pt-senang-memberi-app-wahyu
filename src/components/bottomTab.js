import * as React from 'react';
import {Image, View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Foundation';
import IconFeather from 'react-native-vector-icons/Feather';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-community/async-storage';

function bottomTab(props) {
  const title = props.route.params.title;
  //Pergi menuju halaman pesanan
  const gotoAkun = () => {
    props.navigation.navigate('Akun', {
      title: 'Settings',
      ...props,
    });
  };

  const gotoChat = async () => {
    let syncIg = await AsyncStorage.getItem(`sync-Instagram`);
    let syncWa = await AsyncStorage.getItem(`sync-Whatsapp`);
    let syncFb = await AsyncStorage.getItem(`sync-Mesengger`);
    console.log(syncIg + syncWa + syncFb);
    if (syncIg != null || syncWa != null || syncFb != null) {
      props.navigation.navigate('Chat');
    } else {
      props.navigation.navigate('Chat');
    }
  };

  const gotoHistory = async () => {
    props.navigation.navigate('DonationReport', {title: 'Donation Report'});
  };

  const gotoBantuan = () => {
    props.navigation.navigate('CategoryPage', {title: 'Bantuan Jualan'});
  };

  const gotoHome = () => {
    props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'HomePage', params: {title: 'Jualan Anda'}}],
      }),
    );
  };

  return (
    <View
      style={[
        styles.shadow,
        {
          backgroundColor: '#FDFEFF',
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          paddingTop: 10,
          paddingBottom: 10,
        },
      ]}>
      <TouchableOpacity onPress={gotoHome}>
        <View style={{alignItems: 'center'}}>
          {/* <Icon
            name="home"
            size={30}
            color={title === 'Jualan Anda' ? '#F70161' : '#949494'}
          /> */}
          <Image
            source={
              title === 'Jualan Anda'
                ? require('../assets/images/Icons/PNG/Home-Active.png')
                : require('../assets/images/Icons/PNG/Home-Inactive.png')
            }
            style={{
              width: 30,
              height: 30,
              resizeMode: 'contain',
            }}
          />
          {/* <Text>Home</Text> */}
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={gotoBantuan}>
        <View style={{alignItems: 'center'}}>
          {/* <IconAntDesign
            name="appstore1"
            size={30}
            color={title === 'Bantuan Jualan' ? '#F70161' : '#949494'}
          /> */}
          <Image
            source={
              title === 'Bantuan Jualan'
                ? require('../assets/images/Icons/PNG/Foundation-Active.png')
                : require('../assets/images/Icons/PNG/Foundation.png')
            }
            style={{
              width: 30,
              height: 30,
              resizeMode: 'contain',
            }}
          />
          {/* <Text>Category</Text> */}
        </View>
      </TouchableOpacity>
      {/* <TouchableOpacity onPress={gotoPesanan}>
                <View style={{alignItems:'center'}}>
                    <Icon name="account" size={30} color="#949494" />
                    <Text>Pesanan Saya</Text>
                </View>
            </TouchableOpacity> */}
      <TouchableOpacity onPress={gotoHistory}>
        <View style={{alignItems: 'center'}}>
          {/* <IconFeather
            name="file-text"
            size={30}
            color={title === 'Donation Report' ? '#F70161' : '#949494'}
          /> */}
          <Image
            source={
              title === 'Donation Report'
                ? require('../assets/images/Icons/PNG/History-Active.png')
                : require('../assets/images/Icons/PNG/History.png')
            }
            style={{
              width: 30,
              height: 30,
              resizeMode: 'contain',
            }}
          />
          {/* <Text>History</Text> */}
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={gotoAkun}>
        <View style={{alignItems: 'center'}}>
          {/* <IconFeather
            name="settings"
            size={30}
            color={title === 'Akun Saya' ? '#F70161' : '#949494'}
          /> */}
          <Image
            source={
              title === 'Settings'
                ? require('../assets/images/Icons/PNG/Settings-Active.png')
                : require('../assets/images/Icons/PNG/Settings.png')
            }
            style={{
              width: 30,
              height: 30,
              resizeMode: 'contain',
            }}
          />
          {/* <Text>Settings</Text> */}
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default bottomTab;

const styles = StyleSheet.create({
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
  },
});
