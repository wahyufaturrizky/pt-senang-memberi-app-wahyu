/* eslint-disable */

import React, {useState, useEffect} from 'react';
import {View, Text, Dimensions, Image, Alert, ScrollView} from 'react-native';
import {Avatar} from 'react-native-paper';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-community/async-storage';

import {URL} from '../../utils/global';

import Appbar from '../../components/appbarHome';
import BottomTab from '../../components/bottomTab';
import Loading from '../../components/loading';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

function Akun(props) {
  const placeHolderPhoto =
    'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8d29tYW58ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60';
  const [nama, setNama] = useState('');
  const [phone, setPhone] = useState('');
  const [photo, setPhoto] = useState('');
  const [loading, setLoading] = useState(true);
  const [dataDetail, setDataDetail] = useState([]);

  const {height, width} = Dimensions.get('window');

  const urlProfile = URL + 'v1/my-profile';

  useEffect(() => {
    getProfile();
  }, []);

  const getProfile = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlProfile, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        console.log(responseData.data);
        setNama(responseData.data.fullname);
        setPhone(responseData.data.phone);
        setPhoto(responseData.data.avatar_url);
        setDataDetail(responseData.data);
        setLoading(false);
      });
  };

  const gotoEdit = () => {
    props.navigation.navigate('EditAkun', {
      title: 'Edit Akun',
      data: dataDetail,
    });
  };

  const gotoTarikDana = () => {
    props.navigation.navigate('SaldoPenjual', {title: 'Saldo Penjual'});
  };

  const gotoPembayaran = () => {
    props.navigation.navigate('PembayaranSaya', {title: 'Pembayaran Saya'});
  };

  const gotoInformasiPenjualanSaya = () => {
    props.navigation.navigate('InformasiPenjualanSaya', {
      title: 'Informasi Penjualan Saya',
    });
  };

  const gotoRincianRekening = () => {
    props.navigation.navigate('UpdateAccountProfile', {
      title: 'Edit Profil',
    });
  };

  const gotoWishlist = () => {
    props.navigation.navigate('Wishlist', {title: 'Tambah Produk Saya'});
  };

  const gotoNotifikasi = () => {
    props.navigation.navigate('Notifikasi', {title: 'Notifikasi'});
  };

  const handleLogout = async () => {
    Alert.alert(
      '',
      'Apakah anda yakin ingin keluar?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Ok', onPress: () => logout()},
      ],
      {cancelable: false},
    );
  };

  const logout = async () => {
    await AsyncStorage.removeItem('regular');
    await AsyncStorage.removeItem('data');
    props.navigation.navigate('Splash');
  };

  return (
    <View style={{flex: 1}}>
      <Appbar params={props} />
      <ScrollView>
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <View
            style={{
              flexDirection: 'row',
              padding: 25,
              backgroundColor: '#F8F8F8',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Avatar.Image
              size={height * 0.08}
              source={{uri: placeHolderPhoto}}
            />
            <View style={{marginLeft: width * 0.04}}>
              <Text style={[iOSUIKit.bodyEmphasized, {marginBottom: 8}]}>
                Dwitya Nafa Syafrina
              </Text>
              <Text
                style={[iOSUIKit.body, {color: materialColors.blackTertiary}]}>
                +62 822 7458 6011{phone}
              </Text>

              {/* Disabled Edit account bellow name account*/}
              {/* <TouchableOpacity
                onPress={
                  gotoEdit
                }
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                }}>
                <Icon name="account-edit" size={30} color="#07A9F0" />
                <Text
                  style={{
                    fontSize: 14,
                    color: '#07A9F0',
                    marginLeft: width * 0.01,
                  }}>
                  Ubah Profile
                </Text>
              </TouchableOpacity> */}
            </View>
          </View>

          <View
            style={{
              justifyContent: 'flex-start',
              flexDirection: 'row',
              alignItems: 'center',
              padding: 20,
            }}>
            <Text style={[iOSUIKit.subheadEmphasized]}>Account</Text>
          </View>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <TouchableOpacity onPress={gotoRincianRekening}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: 20,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../../assets/images/Icons/PNG/Profile.png')}
                  style={{
                    width: width * 0.07,
                    height: width * 0.07,
                    marginRight: width * 0.04,
                    resizeMode: 'contain',
                  }}
                />
                <Text style={[iOSUIKit.subhead]}>Your Profile</Text>
              </View>
              <IconMaterialIcons
                name="keyboard-arrow-right"
                size={30}
                color={materialColors.blackTertiary}
              />
            </View>
          </TouchableOpacity>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <TouchableOpacity
            onPress={
              () => alert('Under Constructions')
              // gotoPembayaran
            }>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: 20,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../../assets/images/Icons/PNG/ManagePassword.png')}
                  style={{
                    width: width * 0.07,
                    height: width * 0.07,
                    marginRight: width * 0.04,
                    resizeMode: 'contain',
                  }}
                />
                <Text style={[iOSUIKit.subhead]}>Change Password</Text>
              </View>
              <IconMaterialIcons
                name="keyboard-arrow-right"
                size={30}
                color={materialColors.blackTertiary}
              />
            </View>
          </TouchableOpacity>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <TouchableOpacity
            onPress={
              () => alert('Under Constructions')
              // gotoInformasiPenjualanSaya
            }>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: 20,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../../assets/images/Icons/PNG/Interest.png')}
                  style={{
                    width: width * 0.07,
                    height: width * 0.07,
                    marginRight: width * 0.04,
                    resizeMode: 'contain',
                  }}
                />
                <Text style={[iOSUIKit.subhead]}>Manage Interest</Text>
              </View>
              <IconMaterialIcons
                name="keyboard-arrow-right"
                size={30}
                color={materialColors.blackTertiary}
              />
            </View>
          </TouchableOpacity>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <View
            style={{
              justifyContent: 'flex-start',
              flexDirection: 'row',
              alignItems: 'center',
              padding: 20,
            }}>
            <Text style={[iOSUIKit.subheadEmphasized]}>Support</Text>
          </View>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <TouchableOpacity
            onPress={
              () => alert('Under Constructions')
              // gotoWishlist
            }>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: 20,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../../assets/images/Icons/PNG/FAQ.png')}
                  style={{
                    width: width * 0.07,
                    height: width * 0.07,
                    marginRight: width * 0.04,
                    resizeMode: 'contain',
                  }}
                />
                <Text style={[iOSUIKit.subhead]}>
                  Frequently Asked Question
                </Text>
              </View>
              <IconMaterialIcons
                name="keyboard-arrow-right"
                size={30}
                color={materialColors.blackTertiary}
              />
            </View>
          </TouchableOpacity>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <View
            style={{
              justifyContent: 'flex-start',
              flexDirection: 'row',
              alignItems: 'center',
              padding: 20,
            }}>
            <Text style={[iOSUIKit.subheadEmphasized]}>App Info</Text>
          </View>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <TouchableOpacity
            onPress={
              () => alert('Under Constructions')
              // gotoNotifikasi
            }>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: 20,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../../assets/images/Icons/PNG/Terms.png')}
                  style={{
                    width: width * 0.07,
                    height: width * 0.07,
                    marginRight: width * 0.04,
                    resizeMode: 'contain',
                  }}
                />
                <Text style={[iOSUIKit.subhead]}>Term of Service</Text>
              </View>
              <IconMaterialIcons
                name="keyboard-arrow-right"
                size={30}
                color={materialColors.blackTertiary}
              />
            </View>
          </TouchableOpacity>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <TouchableOpacity
            onPress={
              () => alert('Under Constructions')
              // gotoTarikDana
            }>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: 20,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../../assets/images/Icons/PNG/Policy.png')}
                  style={{
                    width: width * 0.07,
                    height: width * 0.07,
                    marginRight: width * 0.04,
                    resizeMode: 'contain',
                  }}
                />
                <Text style={[iOSUIKit.subhead]}>Privacy Policy</Text>
              </View>
              <IconMaterialIcons
                name="keyboard-arrow-right"
                size={30}
                color={materialColors.blackTertiary}
              />
            </View>
          </TouchableOpacity>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <View
            style={{
              justifyContent: 'flex-start',
              flexDirection: 'row',
              alignItems: 'center',
              padding: 20,
            }}>
            <Text style={[iOSUIKit.subheadEmphasized]}>Others</Text>
          </View>

          <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

          <TouchableOpacity
            onPress={
              // () => alert('Under Constructions')
              handleLogout
            }>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: 20,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../../assets/images/Icons/PNG/Logout.png')}
                  style={{
                    width: width * 0.07,
                    height: width * 0.07,
                    marginRight: width * 0.04,
                    resizeMode: 'contain',
                  }}
                />
                <Text style={[iOSUIKit.subhead]}>Logout</Text>
              </View>
              <IconMaterialIcons
                name="keyboard-arrow-right"
                size={30}
                color={materialColors.blackTertiary}
              />
            </View>
          </TouchableOpacity>
        </View>

        {/* {loading && <Loading />} */}
      </ScrollView>
      <BottomTab {...props} />
    </View>
  );
}

export default Akun;
