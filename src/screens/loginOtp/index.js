import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Modal,
  Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

import Appbar from '../../components/appbarHome';

// React Native Paper Component
import {TextInput} from 'react-native-paper';

// Custome Phone Input by Country Code
import PhoneInput from 'react-native-phone-input';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

// OTP Input
import OTPTextView from 'react-native-otp-textinput';

export default class LoginOtp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalVisibleRegister: false,
      name: '',
      otpInput: '',
    };
  }

  onChangeText = (value, name) => {
    this.setState({
      [name]: value,
    });
  };

  goToHome = () => {
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: 'HomePage',
            params: {
              title: 'Jualan Anda',
              pop: 1,
            },
          },
        ],
      }),
    );
  };

  checkLogin = async () => {
    let value = await AsyncStorage.getItem('data');
    if (value != null) {
      console.log(value);
      this.goToHome();
    }
    console.log(value);
  };

  handleRegular = async () => {
    await AsyncStorage.setItem('regular', 'true');
    this.setState({modalVisible: false});
    this.props.navigation.navigate('Login');
  };

  render() {
    const {modalVisible, modalVisibleRegister, name} = this.state;
    return (
      <View style={styles.container}>
        <Appbar params={this.props} />
        <ImageBackground
          style={styles.backgroundImageStyle}
          source={require('../../assets/images/background_image_login.jpg')}>
          <View style={{position: 'absolute', left: 20, top: 20}}>
            <Text
              style={[
                iOSUIKit.title3Emphasized,
                {
                  marginVertical: 5,
                  color: materialColors.whitePrimary,
                },
              ]}>
              Enter your code
            </Text>
            <Text
              style={[iOSUIKit.caption, {color: materialColors.whitePrimary}]}>
              Please type the code wa sent to +62 822 7458 6011
            </Text>
          </View>

          <OTPTextView
            ref={(e) => (this.input1 = e)}
            containerStyle={styles.textInputContainer}
            textInputStyle={styles.roundedTextInput}
            handleTextChange={(text) => this.setState({otpInput: text})}
            inputCount={4}
            keyboardType="numeric"
            tintColor="#fff"
          />
          <Text style={[material.subheadingWhite, {marginTop: 20}]}>
            Don't receive the code?
          </Text>
          <Text style={[material.subheadingWhite, systemWeights.semibold]}>
            Resend Code (60s)
          </Text>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  roundedTextInput: {
    borderRadius: 100,
    borderWidth: 4,
    backgroundColor: 'white',
  },
  textInputContainer: {
    marginBottom: 20,
  },
  backgroundImageStyle: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // justifyContent: 'center',
    // alignItems: 'center',
    flexDirection: 'column',
  },
  button: {
    borderRadius: 20,
    width: '90%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  buttonWithBorderColor: {
    borderWidth: 2,
    borderColor: 'white',
  },
  buttonText: {
    fontSize: 17,
    textAlign: 'center',
  },
  buttonTextColor: {
    color: '#F70161',
  },
  buttonTextColorWhite: {
    color: '#FFFFFF',
  },
});
