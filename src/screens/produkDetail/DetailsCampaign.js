/* eslint-disable */
import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  Dimensions,
  StyleSheet,
  Picker,
  PermissionsAndroid,
  Modal,
} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import {SliderBox} from 'react-native-image-slider-box';
import HTML from 'react-native-render-html';
import {URL, formatRupiah} from '../../utils/global';
import RNFetchBlob from 'rn-fetch-blob';
import Loading from '../../components/loading';

import SearchableDropdown from 'react-native-searchable-dropdown';

import {ScrollView} from 'react-native-gesture-handler';
import {Title, Snackbar} from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Appbar from '../../components/appbarHome';
import InputNormal from '../../components/inputNormal';

// Icons
import IconFeather from 'react-native-vector-icons/Feather';

// React Native Paper
import {
  Avatar,
  ProgressBar,
  Colors,
  TextInput,
  RadioButton,
} from 'react-native-paper';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

function DetailsCampaign(props) {
  const [dataDetail, setDataDetail] = useState([]);
  const [dataGambar, setDataGambar] = useState([]);
  const [loading, setLoading] = useState(false);

  const [copy, setCopy] = useState(false);
  const [qty, setQty] = useState(1);
  const [postalCode, setPostalCode] = useState(1);

  const [selectVariasi, setSelectVariasi] = useState([]);

  const [pressSize, setPressSize] = useState(false);

  const [selectKota, setSelectKota] = useState([]);
  const [totalKomisi, setTotalKomisi] = useState('0');
  const [service, setService] = useState([]);
  const [serviceFinal, setServiceFinal] = useState('');

  const [varian, setVarian] = useState([]);
  const [courierId, setCourierId] = useState('');
  const [codeCourier, setCodeCourier] = useState('');
  const [kota, setKota] = useState([]);
  const [kecamatan, setKecamatan] = useState([]);
  const [idCity, setIdCity] = useState(0);
  const [est, setEst] = useState('');
  const [totalOngkir, setTotalOngkir] = useState(0);
  const [totalHarga, setTotalHarga] = useState(0);
  const [metodeCOD, setmetodeCOD] = useState(true); //false kalo untuk bank
  const [likeProduk, setLikeProduk] = useState(0);
  const [pilihKota, setPilihKota] = useState(false);
  const [courier, setCourier] = useState([]);
  const [selectedValue, setselectedValue] = useState('Pilih Layanan');

  const urlProdukDetail = URL + 'v1/product/';
  const urlKota = URL + 'v1/shipment/cities';
  const urlCourier = URL + 'v1/courier';
  const urlKecamatan = URL + 'v1/shipment/subdistrict/city';
  const urlOngkir = URL + 'v1/shipment/new/cost/subdistrict';
  const urlWishlistMe = URL + 'v1/wishlist/me?limit=1000000';
  const urlWishlist = URL + 'v1/wishlist';
  const urlKotaDetail = URL + 'v1/shipment/city/';

  const {height, width} = Dimensions.get('window');

  // State Beri App
  const [state, setState] = useState({
    donasi: null,
    lihatSemuaDeskripsi: false,
    modalVisible: false,
    imageSlider: [
      'https://images.unsplash.com/photo-1586293403445-ffa224197984?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8NXx8Y292aWR8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      'https://images.unsplash.com/photo-1584252331038-d4d09c9ebce3?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MjZ8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
      'https://images.unsplash.com/photo-1580458072512-96ced1f43991?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mjl8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
    ],
    kolase: [
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
      {
        imgUrl:
          'https://images.unsplash.com/photo-1605940169839-aedc047b0b06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      },
    ],
    donatur: [
      {
        name: 'Dwitya Nafa Syafrina',
        location: 'Medan',
        amount: '1.245.000',
      },
      {
        name: 'Dwitya Nafa Syafrina',
        location: 'Medan',
        amount: '1.245.000',
      },
    ],
  });

  const placeHolder =
    'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80';
  let id = props.route.params.id;
  console.log('id', id);

  useEffect(() => {
    CekTandai();
    getDetailProduct();
    getKota();
    getCourier();
  }, []);

  const getCourier = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlCourier, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        const mapCourier = responseData.data;
        let data = mapCourier.map((s) => ({
          id: s.id,
          code: s.code,
          name: s.name,
        }));
        setCourier(data);
      })
      .catch((e) => console.log(e));
  };

  const onChangeText = (value, name) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const copyToClipboard = async () => {
    const copyText = `Harga : Rp. ${formatRupiah(
      dataDetail.price_basic,
    )} \n Deskripsi : \n ${dataDetail.description}`;

    const regex = /(<([^>]+)>)/gi;
    const result = copyText.replace(regex, '');

    Clipboard.setString(result);
    setCopy(true);
  };

  const CekTandai = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };
    setLoading(true);
    await fetch(urlWishlistMe, {headers})
      .then((response) => response.json())
      .then(async (responseData) => {
        let res = await responseData.data;
        let row = 1;
        await res.map(async (data, i) => {
          if (data.product_id === id) {
            setLikeProduk(1);
          }
        }, setLoading(false));
      })
      .catch((e) => console.log(e));
  };

  console.log('kkk', likeProduk);

  const changeQty = (simbol) => {
    let hargaProduk = parseInt(
      dataDetail.price_basic +
        dataDetail.price_commission +
        dataDetail.price_benefit,
    );
    let totalOngkirNow = parseInt(totalOngkir);
    let stock = parseInt(dataDetail.stock);

    if (simbol === '+') {
      let qtynow = qty + 1;
      if (qtynow > 1) {
        if (qtynow > stock) {
          alert('Maksimal Quantity adalah ' + stock);
          qtynow = stock;
          setQty(
            qtynow,
            setTotalHarga(hargaProduk * qtynow + totalOngkirNow),
            setTotalKomisi(dataDetail.price_commission * qtynow),
          );
        } else {
          setQty(
            qtynow,
            setTotalHarga(hargaProduk * qtynow + totalOngkirNow),
            setTotalKomisi(dataDetail.price_commission * qtynow),
          );
        }
      } else {
        let qtynow = 1;
        alert('Minimal Quantity adalah 1');
        setQty(
          qtynow,
          setTotalHarga(hargaProduk * qtynow + totalOngkirNow),
          setTotalKomisi(dataDetail.price_commission * qtynow),
        );
      }
    } else if (simbol === '-') {
      let qtynow = qty - 1;
      if (qtynow > 1) {
        if (qtynow > stock) {
          alert('Maksimal Quantity adalah ' + stock);
          qtynow = stock;
          setQty(
            qtynow,
            setTotalHarga(hargaProduk * qtynow + totalOngkirNow),
            setTotalKomisi(dataDetail.price_commission * qtynow),
          );
        } else {
          setQty(
            qtynow,
            setTotalHarga(hargaProduk * qtynow + totalOngkirNow),
            setTotalKomisi(dataDetail.price_commission * qtynow),
          );
        }
      } else {
        qtynow = 1;
        alert('Minimal Quantity adalah 1');
        setQty(
          qtynow,
          setTotalHarga(hargaProduk * qtynow + totalOngkirNow),
          setTotalKomisi(dataDetail.price_commission * qtynow),
        );
      }
    }
  };

  const gotoRincianProduk = () => {
    props.navigation.navigate('WishlistSesungguhnya', {
      title: 'Produk yang Ditandai',
    });
  };

  const gotoPesan = () => {
    let stock = parseInt(dataDetail.stock);
    if (totalOngkir === 0) {
      alert('Pilih Terlebih Kota atau Kecamatan Tujuan');
    } else if (stock < 1) {
      alert('Mohon Maaf.., stok produk ini sedang habis');
    } else {
      console.log('gotoPesan');
      props.navigation.navigate('Pesan', {
        title: 'Pesan & Kirim',
        data: {
          courier_id: dataDetail.is_awb_auto === 1 ? courierId : 1,
          serviceFinal: serviceFinal,
          codeKurir: dataDetail.is_awb_auto === 1 ? codeCourier : 'jne',
          id_produk: id,
          variation: selectVariasi,
          qty,
          metodeCOD,
          totalHarga,
          totalOngkir,
          imageDetail: dataGambar[0],
        },
      });
    }
  };

  const getDetailProduct = async () => {
    setDataGambar([]);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);
    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlProdukDetail + id, {headers})
      .then((response) => response.json())
      .then(async (responseData) => {
        await setDataDetail(responseData.data);

        setTotalKomisi(responseData.data.price_commission);

        if (responseData.data.variation_data != null) {
          setVarian(responseData.data.variation_data);
        }

        let responseImage = responseData.data.images;
        let dataG = '';
        let dataUrl = '';

        responseImage.map(async (data, i) => {
          dataG = dataGambar;
          dataUrl = data.image_url;
          dataG.push(dataUrl);

          await setDataGambar(dataG);
        });
      });
  };

  const checkPermission = async () => {
    if (Platform.OS === 'ios') {
      downloadImage();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: 'This app needs access to your storage to download Photos',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('Storage Permission Granted.');
          downloadImage();
        } else {
          alert('Storage Permission Not Granted');
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };

  const downloadImage = () => {
    let date = new Date();
    let image_URL = dataGambar;

    image_URL.map((url, i) => {
      let ext = getExtention(url);
      ext = '.' + ext[0];
      const {config, fs} = RNFetchBlob;
      let PictureDir = fs.dirs.PictureDir;
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          useDownloadManager: true,
          notification: true,
          path:
            PictureDir +
            '/image_' +
            Math.floor(date.getTime() + date.getSeconds() / 2) +
            ext,
          description: 'Image',
        },
      };
      config(options)
        .fetch('GET', url)
        .then((res) => {
          console.log('res -> ', JSON.stringify(res));
        });
    });

    copyToClipboard();
    alert('Berhasil Mendownload Gambar');
  };

  const getExtention = (filename) => {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
  };

  const getKota = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlKota, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        const mapKota = responseData.rajaongkir.results;
        let data = mapKota.map((s) => ({
          id: s.city_id,
          name: s.type + ' ' + s.city_name,
        }));
        setKota(data);
      })
      .catch((e) => console.log(e));
  };

  const getKecamatan = async (idKec) => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlKecamatan + idKec, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        const mapKota = responseData.rajaongkir.results;
        let data = mapKota.map((s) => ({
          id: s.subdistrict_id,
          name: 'Kecamatan' + ' ' + s.subdistrict_name,
        }));
        setKecamatan(data);
      })
      .catch((e) => console.log(e));
  };

  const _selectKota = async (data_kota) => {
    console.log('data_kota', data_kota);
    await setLoading(true);
    await setIdCity(data_kota.id);
    setPilihKota(false);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    await fetch(`${urlKecamatan}/${data_kota.id}`, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        // console.log('sfsdf', responseData.rajaongkir.results);
        const mapKota = responseData.rajaongkir.results;
        let data = mapKota.map((s) => ({
          id: s.subdistrict_id,
          name: 'Kecamatan' + ' ' + s.subdistrict_name,
        }));
        setKecamatan(data);
      })
      .catch((e) => console.log(e));
    setLoading(false);
    // const value = await AsyncStorage.getItem('data');
    // const data = JSON.parse(value)

    // let headers = {
    //     Authorization: `Bearer ${data.token}`,
    //     'Access-Control-Allow-Origin': '*',
    // }

    fetch(urlKotaDetail + data_kota.id, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        // console.log('response_city',responseData)
        // console.log('dataDetail',dataDetail.cod_city_id)

        // var cod_city = JSON.parse(dataDetail.cod_city_id);
        // var n = cod_city.includes(data_kota.id);

        // // console.log('metodeCod', n)
        // // console.log('cityselect', data_kota.id)

        // if (n) {
        //   setmetodeCOD(true);
        // } else {
        //   setmetodeCOD(false);
        // }
        setPilihKota(true);
      })
      .catch((e) => console.log(e));

    // let formdata = new FormData();
    // formdata.append("origin", dataDetail.city_id)
    // formdata.append("destination", parseInt(data_kota.id))
    // formdata.append("weight", dataDetail.weight)
    // formdata.append("courier", 'jne')

    // fetch(urlOngkir, {method: 'POST', headers,
    //     body: formdata
    // })
    // .then(response => response.json())
    // .then(async(responseData) => {
    //     let tipe= await responseData.rajaongkir.results[0].costs
    //     setLoading(false)
    //     tipe.map((type) => {
    //         if(type.service === "REG"){
    //             setPilihKota(true)
    //             setTotalOngkir(type.cost[0].value, setTotalHarga(dataDetail.price_basic+dataDetail.price_commission+dataDetail.price_benefit+type.cost[0].value))
    //         }
    //     })
    // })
  };

  const setSelectedService = async (dataService) => {
    // const type = service.find(i => i.service === dataService)
    // console.log('okoki', type.cost[0].service);
    setselectedValue(dataService);
    const type = await service.find((i) => i.service === dataService);
    console.log('baji', type);
    setLoading(false);
    setPilihKota(true);
    setServiceFinal(type.service);
    setEst(type.duration);
    setTotalOngkir(parseInt(type.price));
    // console.log("harga total = "+type.cost[0].value+" "+dataDetail.price_basic+" "+dataDetail.price_commission+" "+dataDetail.price_benefit)
    setTotalHarga(
      dataDetail.price_basic +
        dataDetail.price_commission +
        dataDetail.price_benefit +
        parseInt(type.price),
    );
  };

  const _selectCourier = async (data_courier) => {
    const codeKurir = await courier.find((json) => json.id === data_courier.id)
      .code;
    console.log('aq', data_courier, codeKurir);
    await setLoading(true);
    await setCourierId(data_courier.id);
    await setCodeCourier(codeKurir);
    await setLoading(false);
  };

  // console.log('sfsdf', dataDetail);
  const _selectKecamatan = async (data_kota) => {
    console.log('_selectKecamatan', data_kota);
    setLoading(true);

    const value = await AsyncStorage.getItem('data');

    const data = JSON.parse(value);
    console.log('data', data);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };
    fetch(urlKotaDetail + data_kota.id, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        // var cod_city = JSON.parse(dataDetail.cod_city_id)
        console.log(cod_city);
        // var n = cod_city.includes(data_kota.id);
        // console.log('codiii', data_kota.id);
        // if(n) {
        //     setmetodeCOD(true)
        // }else{
        //     setmetodeCOD(false)
        // }
      })
      .catch((e) => console.log(e.response));

    console.log('dataDetail.subdistrict_id', dataDetail.subdistrict_id);
    if (dataDetail.subdistrict_id === null) {
      alert(
        'Alamat asal produk tidak ada, mohon hubungin Customer Service kami di 0878 0229 7802',
      );
      setLoading(false);
    } else {
      let formdata = new FormData();
      formdata.append('origin', parseInt(dataDetail.subdistrict_id));
      formdata.append('destination', parseInt(data_kota.id));
      formdata.append('weight', dataDetail.weight * qty);
      formdata.append('postal_code', postalCode);
      formdata.append(
        'courier',
        dataDetail.is_awb_auto === 1 ? codeCourier : 'jne',
      );
      formdata.append('qty', qty);
      formdata.append('product_id', dataDetail.id);
      formdata.append('is_cod', dataDetail.cod);
      console.log('rtrds', formdata);
      fetch(urlOngkir, {method: 'POST', headers, body: formdata})
        .then((response) => response.json())
        .then(async (responseData) => {
          console.log('uuu', responseData);
          if (responseData.message === 'COD tidak bisa dilakukan') {
            console.log('cek dulu');
            if (dataDetail.is_awb_auto === 1) {
              setmetodeCOD(false);
              setService([]);
              setTotalOngkir(0);
              setTotalHarga(0);
              setLoading(false);
            } else {
              console.log('render ulang');
              setmetodeCOD(false);
              let codFalse = new FormData();
              codFalse.append('origin', parseInt(dataDetail.subdistrict_id));
              codFalse.append('destination', parseInt(data_kota.id));
              codFalse.append('weight', dataDetail.weight * qty);
              codFalse.append('postal_code', postalCode);
              codFalse.append('courier', 'jne');
              codFalse.append('qty', qty);
              codFalse.append('product_id', dataDetail.id);
              codFalse.append('is_cod', 0);
              console.log('rtrds', codFalse);
              fetch(urlOngkir, {method: 'POST', headers, body: codFalse})
                .then((response) => response.json())
                .then(async (responseCod) => {
                  let tipe = await responseCod.data.data;
                  const getJne = (await tipe.find((i) => i.service === 'REG'))
                    ? tipe.find((i) => i.service === 'REG')
                    : tipe.find((i) => i.service === 'CTC');
                  //const getJneSecond = await tipe.find(item => item.service === 'CTC').price
                  const getService = tipe[0].service === 'REG' ? 'REG' : 'CTC';
                  console.log('getjne', getJne);
                  await setService(tipe);
                  await setServiceFinal(getService);
                  await setEst(tipe[0].duration);
                  await setTotalOngkir(parseInt(getJne.price));
                  // console.log("harga total = "+type.cost[0].value+" "+dataDetail.price_basic+" "+dataDetail.price_commission+" "+dataDetail.price_benefit)
                  await setTotalHarga(
                    dataDetail.price_basic +
                      dataDetail.price_commission +
                      dataDetail.price_benefit +
                      parseInt(getJne.price),
                  );
                })
                .catch((e) => {
                  console.log('check harga produk', e);
                });
              setLoading(false);
            }
          } else if (responseData.data.data[0].errors) {
            setmetodeCOD(false);
            setTotalOngkir(0);
            setTotalHarga(0);
            setService([]);
            setLoading(false);
          } else {
            let tipe = await responseData.data.data;
            const getJne = (await tipe.find((i) => i.service === 'REG'))
              ? tipe.find((i) => i.service === 'REG')
              : tipe.find((i) => i.service === 'CTC');
            //const getJneSecond = await tipe.find(item => item.service === 'CTC').price
            const getService = tipe[0].service === 'REG' ? 'REG' : 'CTC';
            console.log('getjne', getJne);
            await setmetodeCOD(true);
            await setService(tipe);
            await setServiceFinal(
              dataDetail.is_awb_auto === 1 ? tipe[0].service : getService,
            );
            await setEst(tipe[0].duration);
            await setTotalOngkir(
              dataDetail.is_awb_auto === 1
                ? parseInt(tipe[0].price)
                : parseInt(getJne.price),
            );
            // console.log("harga total = "+type.cost[0].value+" "+dataDetail.price_basic+" "+dataDetail.price_commission+" "+dataDetail.price_benefit)
            await setTotalHarga(
              dataDetail.is_awb_auto === 1
                ? dataDetail.price_basic +
                    dataDetail.price_commission +
                    dataDetail.price_benefit +
                    parseInt(tipe[0].price)
                : dataDetail.price_basic +
                    dataDetail.price_commission +
                    dataDetail.price_benefit +
                    parseInt(getJne.price),
            );
            setLoading(false);
          }
          // tipe.map(type => {
          //   if (type.service === 'REG' || type.service === 'CTC') {
          //     setLoading(false);
          //     setPilihKota(true);
          //     setEst(type.cost[0].etd);
          //     setTotalOngkir(type.cost[0].value);
          //     // console.log("harga total = "+type.cost[0].value+" "+dataDetail.price_basic+" "+dataDetail.price_commission+" "+dataDetail.price_benefit)
          //     setTotalHarga(
          //       dataDetail.price_basic +
          //         dataDetail.price_commission +
          //         dataDetail.price_benefit +
          //         type.cost[0].value,
          //     );
          //   }
          // });
        });
    }
  };

  // console.log('est', est);

  const postWishlist = async (id) => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);
    const id_user = data.id;

    var formdata = new FormData();
    formdata.append('product_id', id);
    formdata.append('user_id', id_user);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'multipart/form-data',
    };

    fetch(urlWishlist, {
      method: 'POST',
      headers,
      body: formdata,
    })
      .then((response) => response.json())
      .then(async (responseData) => {
        console.log(responseData);
        setLoading(false);
        alert(responseData.message);
        gotoRincianProduk();
      })
      .catch((e) => console.log(e))
      .done();
  };

  const _onDismissSnackBar = () => setCopy(false);

  const addVariasi = (variant, value) => {
    let data = [{[variant]: value}];
    let allVariasi = selectVariasi;

    allVariasi.forEach(function (v) {
      delete v[variant];
    });

    let filtered = allVariasi.filter((value) => JSON.stringify(value) !== '{}');

    setSelectVariasi(filtered.concat(data));
  };

  console.log('Kecamatan', metodeCOD);

  const {
    kolase,
    donatur,
    imageSlider,
    modalVisible,
    donasi,
    lihatSemuaDeskripsi,
  } = state;

  return (
    <View style={{backgroundColor: '#F8F8FB', flex: 1}}>
      <Appbar params={props} like={likeProduk < 1 ? false : true} />

      <ScrollView keyboardShouldPersistTaps="always">
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginBottom: height * 0.02,
            flex: 1,
          }}>
          <SliderBox
            // paginationBoxVerticalPadding={10}
            images={imageSlider}
            onCurrentImagePressed={(index) =>
              console.warn(`image ${index} pressed`)
            }
            currentImageEmitter={(index) =>
              console.warn(`current pos is: ${index}`)
            }
            dotColor="#F70161"
            inactiveDotColor="#90A4AE"
            sliderBoxHeight={200}
            resizeMode={'cover'}
            ImageComponentStyle={{
              borderRadius: 15,
              marginTop: 0,
              marginBottom: 0,
            }}
            imageLoadingColor="#F70161"
            resizeMethod={'resize'}
            // style={{
            //   width: '90%',
            //   // height: height * 0.4,
            //   // resizeMode: 'contain',
            // }}
          />

          <View>
            <Text style={[iOSUIKit.bodyEmphasized, {marginTop: 24}]}>
              Bantuan Hazmat Relawan Convid 19
            </Text>
          </View>
          <View
            style={{
              marginTop: 24,
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View>
              <Text
                style={[
                  iOSUIKit.caption2Emphasized,
                  {color: materialColors.blackTertiary},
                ]}>
                Dibuat oleh
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginTop: 14,
                }}>
                <Avatar.Image
                  size={height * 0.04}
                  source={{uri: placeHolder}}
                />
                <Text style={[{marginLeft: 12}, iOSUIKit.caption2]}>
                  Martina Brito
                </Text>
              </View>
            </View>
            <View>
              <Text
                style={[
                  iOSUIKit.caption2Emphasized,
                  {color: materialColors.blackTertiary},
                ]}>
                Sisa waktu
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginTop: 14,
                }}>
                <IconFeather
                  name="calendar"
                  size={height * 0.04}
                  color="#C4C4C4"
                />
                <Text style={[{marginLeft: 12}, iOSUIKit.caption2]}>
                  29 hari lagi
                </Text>
              </View>
            </View>
          </View>

          <View
            style={{
              borderColor: materialColors.blackTertiary,
              borderWidth: 2,
              borderRadius: 12,
              padding: 15,
              marginTop: 24,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text style={[iOSUIKit.footnoteEmphasized]}>Rp 142.686.812</Text>
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackTertiary},
                ]}>
                70% dari 200 juta
              </Text>
            </View>
            <ProgressBar
              style={{marginTop: 12}}
              progress={80 / 100}
              color={Colors.red800}
            />
          </View>

          <View style={{marginTop: 24}}>
            <Text style={[iOSUIKit.bodyEmphasized]}>Deskripsi</Text>
            <Text style={[iOSUIKit.subhead, {marginTop: 14}]}>
              Virus corona (COVID-19) adalah virus yang{' '}
              {lihatSemuaDeskripsi && (
                <Text>
                  Virus corona (COVID-19) adalah virus yang menyerang sistem
                  pernapasan dengan efek yang tidak bisa disepelekan. Semakin
                  hari, jumlah pasien yang terinfeksi positif dan meningga;
                  terus bertambah, termasuk tenaga Virus corona (COVID-19)
                  adalah virus yang{' '}
                </Text>
              )}
            </Text>
            <TouchableOpacity
              onPress={(value) =>
                onChangeText(!lihatSemuaDeskripsi, 'lihatSemuaDeskripsi')
              }>
              <Text style={{color: '#F70161'}}>
                {lihatSemuaDeskripsi ? 'perkecil penjelasan' : 'Lihat semua'}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{marginTop: 24}}>
            <Text style={[iOSUIKit.bodyEmphasized]}>Kolase Foto</Text>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              style={{marginVertical: 14}}>
              {kolase.map((value, key) => {
                const {imgUrl} = value;
                return (
                  <Image
                    key={key}
                    source={{
                      uri: `${imgUrl}`,
                    }}
                    style={{
                      width: width * 0.3,
                      height: width * 0.3,
                      resizeMode: 'cover',
                      borderRadius: 20,
                      marginRight: 12,
                    }}
                  />
                );
              })}
            </ScrollView>
          </View>

          <View style={{marginTop: 24}}>
            <Text style={[iOSUIKit.bodyEmphasized]}>Donatur</Text>
            {donatur.map((value, key) => {
              const {name, location, amount} = value;
              return (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginTop: 12,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <Avatar.Image
                      source={{uri: placeHolder}}
                      size={height * 0.05}
                    />
                    <View>
                      <Text
                        style={[{marginLeft: 12}, iOSUIKit.footnoteEmphasized]}>
                        {name}
                      </Text>
                      <Text
                        style={[
                          iOSUIKit.caption2,
                          {
                            marginLeft: 12,
                            marginTop: 4,
                            color: materialColors.blackTertiary,
                          },
                        ]}>
                        {location}
                      </Text>
                    </View>
                  </View>
                  <View>
                    <Text>Rp {amount}</Text>
                  </View>
                </View>
              );
            })}
          </View>
        </View>
      </ScrollView>

      <View style={{alignItems: 'flex-end'}}>
        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate('InputNominalDonate', {
              title: 'Input Nominal Donate',
            })
          }>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            colors={['#F70161', '#d6245a', '#d6245a']}
            style={{
              padding: 10,
              margin: 0,
              borderRadius: 12,
              justifyContent: 'center',
              margin: 8,
              paddingVertical: 10,
              paddingHorizontal: 18,
            }}>
            <Text
              style={[
                iOSUIKit.footnote,
                {textAlign: 'center', color: 'white'},
              ]}>
              Donate
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>

      {/* Deactive loading */}
      {/* {loading && <Loading />} */}

      {/* ----- [START BUTTON BOTTOM OF PAGE] ----- */}
      {/* <TouchableOpacity
        onPress={() => setState({...state, modalVisible: true})}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          colors={['#F70161', '#F70161', '#FE4D91']}
          style={{padding: 15, margin: 15, borderRadius: 12}}>
          <Text style={{fontSize: 24, textAlign: 'center', color: 'white'}}>
            Donate
          </Text>
        </LinearGradient>
      </TouchableOpacity> */}
      {/* ----- [END BUTTON BOTTOM OF PAGE] ----- */}

      <Snackbar visible={copy} onDismiss={_onDismissSnackBar} duration={1000}>
        Deskripsi Berhasil di Salin
      </Snackbar>

      {/* [START MODAL INPUT NOMINAL] */}
      {/* <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setState({
            ...state,
            modalVisible: false,
          });
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => {
                setState({
                  ...state,
                  modalVisible: false,
                });
              }}>
              <Image
                style={{
                  borderColor: '#16212D',
                  borderWidth: 2,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                source={require('../../assets/images/close.png')}
              />
            </TouchableOpacity>
            <Text style={[styles.modalText, material.title]}>
              Nominal Donasi
            </Text>
            <Text
              style={[
                styles.modalText,
                material.subheading,
                {
                  color: materialColors.blackTertiary,
                },
              ]}>
              *Min. Donasi Rp 10.000
            </Text>

            <View style={{flex: 1, marginTop: 24}}>
              <TextInput
                mode="outlined"
                theme={{
                  colors: {
                    primary: '#F70161',
                    underlineColor: 'transparent',
                  },
                }}
                label="input donasi"
                value={donasi}
                keyboardType="numeric"
                onChangeText={donasi => onChangeText(donasi, 'donasi')}
              />
            </View>

            <TouchableOpacity
              style={{
                ...styles.openButton,
                backgroundColor: '#F70161',
              }}
              // onPress={() => this.handleRegular()}
              onPress={() => {
                props.navigation.navigate('DetailsCampaign', {
                  title: 'Choose Your Plan',
                });
                setState({
                  ...state,
                  modalVisible: false,
                });
              }}>
              <Text style={[styles.textStyle]}>Selanjutnya</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal> */}
    </View>
  );
}

export default DetailsCampaign;

const styles = StyleSheet.create({
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    marginBottom: 20,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  centeredView: {
    // flex: 1,
    height: 400,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
