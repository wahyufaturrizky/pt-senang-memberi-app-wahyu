/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  RefreshControl,
} from 'react-native';
import {
  Text,
  Card,
  Chip,
  Avatar,
  ProgressBar,
  Colors,
} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFeather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import {URL} from '../../utils/global';
import {name as app_name, version as app_version} from '../../../package.json';

import Appbar from '../../components/appbarHome';
import AppbarT from '../../components/appBarTransparent';
import BottomTab from '../../components/bottomTab';
import Loading from '../../components/loading';
import VersionCheck from 'react-native-version-check';
import {ScrollView} from 'react-native-gesture-handler';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function HomePage(props) {
  const [refreshing, setRefreshing] = useState(false);
  const [wishlist, setWishlist] = useState(null);
  const [jumlahProdukDitandai, setJumlahProdukDitandai] = useState(null);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState('');
  const [saldo, setSaldo] = useState('0');
  const [totalOrder, setTotalOrder] = useState([]);
  const [jumlahPesanan, setJumlahPesanan] = useState(null);
  const [jumlahKategori, setJumlahKategori] = useState(null);
  const [popup, setPopup] = useState([]);
  const [modal, setModal] = useState(true);
  const [notif, setNotif] = useState(0);

  const {height, width} = Dimensions.get('window');
  const haveProduk = true;
  const urlWishlist = URL + 'v1/wishlist/me';
  const urlCategory = URL + 'v1/category';
  const urlSaldo = URL + 'v1/saldo/my';
  const urlTotalOrder = URL + 'v1/orders/my-order?status=5';
  const urlPopup = URL + 'v1/popup';
  const urlNotif = URL + 'v1/notification/me';

  // State Beri App
  const [state, setState] = useState({
    categoryDonate: [
      {
        name_category: 'Poverty',
        icon: 'https://www.linkpicture.com/q/donation_1.png',
      },
      {
        name_category: 'Health',
        icon: 'https://www.linkpicture.com/q/mask.png',
      },
      {
        name_category: 'Education',
        icon: 'https://www.linkpicture.com/q/education_2.png',
      },
      {
        name_category: 'Equality',
        icon: 'https://www.linkpicture.com/q/equality.png',
      },
      {
        name_category: 'Animals',
        icon: 'https://www.linkpicture.com/q/animals.png',
      },
      {
        name_category: 'Environment',
        icon: 'https://www.linkpicture.com/q/environtment.png',
      },
      {
        name_category: 'Music',
        icon: 'https://www.linkpicture.com/q/image-6_5.png',
      },
    ],
    thumbDonate: [
      {
        title: 'Bantuan hazmat relawan',
        terkumpul: 80000000,
        target: 100000000,
        sisa_hari: 29,
        imageUrl:
          'https://images.unsplash.com/photo-1586293403445-ffa224197984?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8Y292aWR8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title: 'Bantuan hazmat relawan',
        terkumpul: 90000000,
        target: 100000000,
        sisa_hari: 29,
        imageUrl:
          'https://images.unsplash.com/photo-1586293403445-ffa224197984?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8Y292aWR8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title: 'Bantuan hazmat relawan',
        terkumpul: 90000000,
        target: 100000000,
        sisa_hari: 29,
        imageUrl:
          'https://images.unsplash.com/photo-1586293403445-ffa224197984?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8Y292aWR8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title: 'Bantuan hazmat relawan',
        terkumpul: 80000000,
        target: 100000000,
        sisa_hari: 29,
        imageUrl:
          'https://images.unsplash.com/photo-1586293403445-ffa224197984?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8Y292aWR8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title: 'Bantuan hazmat relawan',
        terkumpul: 90000000,
        target: 100000000,
        sisa_hari: 29,
        imageUrl:
          'https://images.unsplash.com/photo-1586293403445-ffa224197984?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8Y292aWR8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title: 'Bantuan hazmat relawan',
        terkumpul: 90000000,
        target: 100000000,
        sisa_hari: 29,
        imageUrl:
          'https://images.unsplash.com/photo-1586293403445-ffa224197984?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8Y292aWR8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
    ],
    news: [
      {
        title:
          'Beberapa influencer siapkan acara galang dana untuk korban covid',
        like: 34,
        sub_title: 'In this modern day bussiness is more...',
        imageUrl:
          'https://images.unsplash.com/photo-1603843722974-3a4031f9f97c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTZ8fGNvdmlkJTIwMTl8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title:
          'Beberapa influencer siapkan acara galang dana untuk korban covid',
        like: 34,
        sub_title: 'In this modern day bussiness is more...',
        imageUrl:
          'https://images.unsplash.com/photo-1603843722974-3a4031f9f97c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTZ8fGNvdmlkJTIwMTl8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title:
          'Beberapa influencer siapkan acara galang dana untuk korban covid',
        like: 34,
        sub_title: 'In this modern day bussiness is more...',
        imageUrl:
          'https://images.unsplash.com/photo-1603843722974-3a4031f9f97c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTZ8fGNvdmlkJTIwMTl8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title:
          'Beberapa influencer siapkan acara galang dana untuk korban covid',
        like: 34,
        sub_title: 'In this modern day bussiness is more...',
        imageUrl:
          'https://images.unsplash.com/photo-1603843722974-3a4031f9f97c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTZ8fGNvdmlkJTIwMTl8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title:
          'Beberapa influencer siapkan acara galang dana untuk korban covid',
        like: 34,
        sub_title: 'In this modern day bussiness is more...',
        imageUrl:
          'https://images.unsplash.com/photo-1603843722974-3a4031f9f97c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTZ8fGNvdmlkJTIwMTl8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title:
          'Beberapa influencer siapkan acara galang dana untuk korban covid',
        like: 34,
        sub_title: 'In this modern day bussiness is more...',
        imageUrl:
          'https://images.unsplash.com/photo-1603843722974-3a4031f9f97c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTZ8fGNvdmlkJTIwMTl8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title:
          'Beberapa influencer siapkan acara galang dana untuk korban covid',
        like: 34,
        sub_title: 'In this modern day bussiness is more...',
        imageUrl:
          'https://images.unsplash.com/photo-1603843722974-3a4031f9f97c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTZ8fGNvdmlkJTIwMTl8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
    ],
  });

  let pop = 0;
  if (props.route.params.pop != null) {
    pop = props.route.params.pop;
  }

  useEffect(() => {
    getVersion();
    getListWishlist();
    getSaldo();
    getTotalOrder();
    // --- [Deactive] ---
    // getPopup();
    // --- [Deactive] ---
    getNotif();
    getKategori();
  }, []);

  //Pergi ke Hal List Produk
  const listProduk = (title) => {
    props.navigation.navigate('Produk', {title});
  };

  const getVersion = () => {
    VersionCheck.getLatestVersion().then((latestVersion) => {
      console.log(latestVersion); // 0.1.2
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getVersion();
    getListWishlist();
    getSaldo();
    getTotalOrder();
    getPopup();
    getNotif();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  //Pergi ke Hal Pesanan
  const gotoPesanan = () => {
    props.navigation.navigate('PesananSaya', {title: 'Pesanan Saya'});
  };

  const gotoLihatDetailsCampaign = () => {
    props.navigation.navigate('DetailsCampaign', {title: 'Detail Campaign'});
  };

  //Pergi ke Hal List Kategori
  const gotoKategori = () => {
    props.navigation.navigate('Kategori', {title: 'Produk Lain'});
  };

  //Pergi ke Hal List Wishlist
  const gotoWishlist = () => {
    props.navigation.navigate('WishlistNoButtonTambah', {
      title: 'Tambah Produk Saya',
    });
  };

  const gotoPalingDisukaiSesungguhnya = () => {
    props.navigation.navigate('WishlistSesungguhnya', {
      title: 'Produk yang Ditandai',
    });
  };

  const getNotif = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);
    console.log(data.token);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    console.log(urlNotif + '?order_direction=desc');

    fetch(urlNotif + '?order_direction=desc', {headers})
      .then((response) => response.json())
      .then((responseData) => {
        let a = 0;
        let data = responseData.data;
        // console.log(responseData.data)
        // data.reverse()
        data.map((val, i) => {
          console.log(val.id + 'status = ' + val.status);
          if (val.status == 0) {
            a++;
          }
        });
        setNotif(a);
      });
  };

  const getKategori = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlCategory + '?limit=9&offset=0', {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlCategory', totalData);
        setJumlahKategori(responseData.meta.total_data);
      })
      .catch((e) => console.log(e));
  };

  //Untuk Ngecek Berapa saldonya
  const getSaldo = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlSaldo, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setLoading(false);
        setSaldo(responseData.data);
      })
      .catch((e) => console.log(e));
  };

  //Untuk Ngecek udah ada wishlist apa belum
  const getListWishlist = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlWishlist, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlWishlist', responseData);
        setWishlist(totalData);
        setJumlahProdukDitandai(responseData.meta.total_data);
      })
      .catch((e) => console.log(e));
  };

  //Untuk dapetin udah berapa banyak yang order
  const getTotalOrder = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlTotalOrder, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        console.log('yyyy', responseData);
        setLoading(false);
        setTotalOrder(responseData.data);
        setJumlahPesanan(responseData.meta.total_data);
      })
      .catch((e) => console.log(e));
  };

  console.log('sfsdfs', totalOrder);

  //Pergi ke Hal Cari Produk
  const searchProduk = () => {
    props.navigation.navigate('Produk', {title: 'Cari Produk', search: search});
  };

  const getPopup = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlPopup, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        // console.log(responseData.data)
        setPopup(responseData.data);
        if (pop == 1) setModal(true);
        else {
          setModal(false);
        }
      })
      .catch((e) => console.log(e));
  };

  const modalTrigger = async () => {
    setModal(!modal);
  };

  const {categoryDonate, thumbDonate, news} = state;

  return (
    <View style={{flex: 1, backgroundColor: '#F8F8FB'}}>
      {/* {wishlist > 0 ? ( */}

      {/* ----- [Deactive AppBar] ----- */}
      {/* <AppbarT
        params={props}
        haveProduk={haveProduk}
        notif={notif}
        wishlist={wishlist}
      /> */}

      {/* ) : (
        <Appbar
          params={props}
          haveProduk={haveProduk}
          notif={notif}
          wishlist={wishlist}
        />
      )} */}

      {/* {wishlist < 1 ? (
        <Image
          source={require('../../assets/images/banner-home.png')}
          style={{width: width * 1, height: height * 0.3}}
          width={width * 1}
          height={height * 0.25}
          resizeMode="contain"
        />
      ) : ( */}
      <ImageBackground
        source={require('../../assets/images/banner-home-white.png')}
        style={{justifyContent: 'flex-start', height: height * 0.15}}>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: height * 0.03,
          }}>
          <Text style={[iOSUIKit.subhead, {marginVertical: 5}]}>
            Halo,{' '}
            <Text style={[systemWeights.semibold]}>Dwitya nafa Syafrina</Text>
          </Text>
          <Text
            style={[
              iOSUIKit.title3Emphasized,
              {marginVertical: 5, fontWeight: '700'},
            ]}>
            Mau donasi apa hari ini?
          </Text>
        </View>
      </ImageBackground>
      {/* )} */}
      {/* {wishlist > 0 && ( */}
      <View
        style={[
          styles.shadow,
          {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#fff',
            borderRadius: 10,
            bottom: 10,
            width: '90%',
            alignSelf: 'center',
            marginBottom: 10,
          },
        ]}>
        <Icon style={{padding: 10}} name="magnify" size={20} color="#949494" />
        <TextInput
          style={{
            flex: 1,
            paddingTop: 10,
            paddingRight: 10,
            paddingBottom: 10,
            paddingLeft: 0,
            color: '#424242',
            height: 50,
          }}
          onChangeText={(val) => setSearch(val)}
          placeholder="Masukkan kata kunci donasi"
          underlineColorAndroid="transparent"
          onSubmitEditing={
            () => alert('Under Constractions')
            // searchProduk
          }
        />

        {/* --- [Deactive Feature Camera] --- */}
        {/* <Icon style={{padding: 10}} name="camera" size={30} color="#07A9F0" /> */}
      </View>

      {/* )} */}
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: height * -0.02,
            flex: 1,
          }}>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            style={{marginBottom: 10, marginTop: 15, width: '105%'}}
            horizontal={true}>
            {categoryDonate.map((value, key) => {
              const {icon, name_category} = value;
              return (
                <Chip
                  style={{
                    marginRight: 12,
                    borderColor: '#F70161',
                    borderWidth: 1,
                    padding: 8,
                    fontWeight: 700,
                  }}
                  avatar={
                    <Avatar.Image
                      style={{
                        backgroundColor: 'white',
                        alignItems: 'center',
                        flexDirection: 'row',
                        marginRight: 5,
                      }}
                      size={height * 0.03}
                      // size={8}
                      // for URL
                      // source={{uri: photo}}
                      // for image from storage
                      source={{uri: icon}}
                    />
                  }
                  key={key}
                  mode="outlined"
                  onPress={() => alert('Under Constructions')}>
                  {name_category}
                </Chip>
              );
            })}
          </ScrollView>

          <ScrollView
            showsHorizontalScrollIndicator={false}
            style={{marginBottom: 20, marginTop: 20, width: '105%'}}
            horizontal={true}>
            {thumbDonate.map((value, key) => {
              const {title, terkumpul, target, sisa_hari, imageUrl} = value;
              return (
                <TouchableOpacity
                  style={{
                    marginRight: 12,
                    width: height * 0.3,
                  }}
                  onPress={() => {
                    gotoLihatDetailsCampaign();
                  }}>
                  <ImageBackground
                    imageStyle={{borderRadius: 12}}
                    source={{uri: imageUrl}}
                    resizeMode="cover"
                    style={{
                      justifyContent: 'flex-end',
                      padding: 10,
                      height: height * 0.2,
                    }}>
                    <View
                      style={{
                        marginLeft: width * 0.02,
                        marginBottom: width * 0.03,
                        borderRadius: 6,
                        paddingVertical: 6,
                        width: '45%',
                        backgroundColor: 'white',
                      }}>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: width * 0.03,
                        }}>
                        {sisa_hari} hari lagi
                      </Text>
                    </View>
                  </ImageBackground>
                  <Text style={[iOSUIKit.subheadEmphasized, {marginTop: 8}]}>
                    {title}
                  </Text>
                  <ProgressBar
                    style={{marginTop: 8}}
                    progress={terkumpul / target}
                    color={Colors.red800}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      marginTop: 8,
                    }}>
                    <Text style={{fontSize: 10}}> Terkumpul </Text>
                    <Text style={{fontSize: 10, fontWeight: 'bold'}}>
                      Rp {terkumpul}
                      <Text> ({(terkumpul / target) * 100}%)</Text>
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          </ScrollView>

          <TouchableOpacity
            onPress={() => {
              alert('this space for advertising');
              // listProduk('Produk Terlaris')
            }}
            style={{marginTop: height * 0.01, marginBottom: 20}}>
            <Card>
              <Card.Cover
                source={require('../../assets/images/banner-beri.png')}
                style={{height: height * 0.2, resizeMode: 'cover'}}
              />
              {/* <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  padding: 10,
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 16}}>Produk Deplaza</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Icon name="heart" size={16} color="#707070" />
                  <Text style={{fontSize: 10}}> 200 Orang </Text>
                </View>
              </View> */}
            </Card>
          </TouchableOpacity>

          {/* {wishlist > 0 && ( */}
          {/* <TouchableOpacity onPress={gotoWishlist} style={{marginBottom: 10}}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              colors={['#0956C6', '#0879D8', '#07A9F0']}
              style={{
                padding: 15,
                flexDirection: 'row',
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={require('../../assets/images/box2.png')}
                style={{width: width * 0.05, height: width * 0.05}}
              />
              <Text
                style={{
                  fontSize: 18,
                  textAlign: 'center',
                  color: 'white',
                  marginLeft: width * 0.04,
                }}>
                Produk Saya
              </Text>
            </LinearGradient>
          </TouchableOpacity> */}
          {/* )} */}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: 8,
              marginBottom: 20,
            }}>
            <Text style={iOSUIKit.title3Emphasized}> Berita dari Beri </Text>
            <Text style={[iOSUIKit.subheadEmphasized, {color: '#F70161'}]}>
              Lihat semua
            </Text>
          </View>

          {news.map((value, key) => {
            const {imageUrl, title, sub_title, like} = value;
            return (
              <TouchableOpacity
                key={key}
                onPress={() => alert('Under Constructions')}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: height * 0.01,
                    backgroundColor: 'white',
                    borderRadius: 12,
                    padding: 15,
                  }}>
                  <View style={{width: '32%'}}>
                    <Image
                      source={{uri: imageUrl}}
                      resizeMode="cover"
                      style={{
                        height: height * 0.2,
                        borderRadius: 12,
                      }}
                    />
                  </View>

                  <View style={{width: '64%'}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        marginTop: 8,
                      }}>
                      <Text
                        style={{
                          fontSize: 10,
                          color: '#F70161',
                          fontWeight: 'bold',
                        }}>
                        {' '}
                        Donasi{' '}
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <IconFeather
                          name="calendar"
                          size={width * 0.03}
                          color="#C4C4C4"
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: 'bold',
                          }}>
                          {' '}
                          2 day ago
                        </Text>
                        <IconFeather
                          style={{marginLeft: 10}}
                          name="trello"
                          size={width * 0.03}
                          color="#C4C4C4"
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: 'bold',
                          }}>
                          {' '}
                          {value.like}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        justifyContent: 'flex-start',
                        marginTop: 10,
                        height: height * 0.1,
                      }}>
                      <Text
                        style={{
                          color: materialColors.blackPrimary,
                          fontSize: width * 0.04,
                          marginLeft: width * 0.01,
                          fontWeight: 'bold',
                        }}>
                        {value.title}
                      </Text>
                      <Text
                        style={{
                          color: materialColors.blackTertiary,
                          fontSize: width * 0.03,
                          marginLeft: width * 0.01,
                          marginTop: 10,
                        }}>
                        {value.sub_title}
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
      {/* {loading && <Loading />} */}

      <BottomTab {...props} />

      {modal
        ? popup.map((data, i) => (
            <View
              key={i}
              style={{
                position: 'absolute',
                flex: 1,
                zIndex: 2,
                width: width,
                height: height,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  styles.shadow,
                  {
                    alignSelf: 'center',
                    width: width * 0.6,
                    backgroundColor: 'rgba(255,255,255,1)',
                    padding: 15,
                  },
                ]}>
                <Text style={{textAlign: 'center', marginBottom: 10}}>
                  {data.name}
                </Text>
                <Image
                  source={{uri: data.image_url}}
                  style={{
                    width: '80%',
                    alignSelf: 'center',
                    height: height * 0.3,
                    resizeMode: 'contain',
                  }}
                />

                <TouchableOpacity
                  style={{alignSelf: 'flex-end'}}
                  onPress={() => modalTrigger()}>
                  <Text style={{fontSize: 14, color: '#07A9F0'}}>Tutup</Text>
                </TouchableOpacity>
              </View>
            </View>
          ))
        : null}
    </View>
  );
}

export default HomePage;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
